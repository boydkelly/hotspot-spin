%post
cat >>  /etc/dnsmasq.conf << FOE
log-facility=/var/log/dnsmasq.log
address=/#/10.2.0.1
interface=wlp1s0
dhcp-range=10.2.0.10,10.2.0.250,12h
#no-resolv
log-queries
server=10.2.0.1
FOE

cat > /etc/hosts <<FOE
127.0.0.1   localhost videos localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost videos localhost.localdomain localhost6 localhost6.localdomain6
FOE

%end

