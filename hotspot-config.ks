%post

#hostapd
cat > /etc/hostapd/hostapd.conf <<FOE
#
# This will give you a minimal, insecure wireless network.
# 
# DO NOT BE SATISFIED WITH THAT!!!
#
# A complete, well commented example configuration file is
# available here:
#
#	/usr/share/doc/hostapd/hostapd.conf
#
# For more information, look here:
#
#	http://wireless.kernel.org/en/users/Documentation/hostapd
#

ctrl_interface=/var/run/hostapd
ctrl_interface_group=wheel

# Some usable default settings...
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0

# Uncomment these for base WPA & WPA2 support with a pre-shared key
#wpa=3
#wpa_key_mgmt=WPA-PSK
#wpa_pairwise=TKIP
#rsn_pairwise=CCMP

# DO NOT FORGET TO SET A WPA PASSPHRASE!!
#wpa_passphrase=YourPassPhrase

# Most modern wireless drivers in the kernel need driver=nl80211
driver=nl80211

# Customize these for your local configuration...
interface=wlp1s0
hw_mode=g
channel=0
ssid=VIDEOS
FOE


#dnsmasq
cat >>  /etc/dnsmasq.conf << FOE
log-facility=/var/log/dnsmasq.log
address=/#/10.2.0.1
interface=wlp1s0
dhcp-range=10.2.0.10,10.2.0.250,12h
#no-resolv
log-queries
server=10.2.0.1
FOE

cat > /etc/hosts <<FOE
127.0.0.1   localhost videos localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost videos localhost.localdomain localhost6 localhost6.localdomain6
FOE


#nginx
cat > /etc/nginx/default.d/videos.conf << FOE
server {
	listen 80 default_server;
	listen [::]:80 default_server ipv6only=on;
	server_name videos.localhost www.videos.localhost;
	location / {
	  root /home/liveuser/Videos;
	  index index.html index.htm;
	}
}
FOE

sudo sed -i 's/\/usr\/share\/nginx\/html/\/home\/liveuser\/Videos/
;' /etc/nginx/nginx.conf                                          
sudo sed -i '/root/a\t autoindex on'                              
#systemctl restart nginx                


#what is this?  It may have to go elsewhere
sed -i 's/^i[[:space:]]root/\t\troot\t\/home\/liveuser\/Videos/' /etc/nginx/default.conf


#samba
cat > /etc/samba/smb.conf << FOE
[global]
 workgroup = bouake
 security = user
 share modes = yes
 null passwords = yes

 [videos]
 path = /home/liveuser/Videos/ 
 public = yes
 writable = yes
 comment = smb share
 printable = no
 guest ok = yes
FOE

cat > /etc/samba/smbusers << FOE
liveuser
videos
FOE

#iwconfig  hmmm....  does this go at the end or start?
#or is it alternative to nmcli it was commented out in main file
#iwconfig wlp1s0 essid videos mode master
#ip address dev wlp1s0 10.2.0.1 netmask 255.255.255.0
#ifconfig wlp1s0 10.2.0.1 netmask 255.555.255.0 broadcast 10.2.0.255 up
# iptables-restore < saved-hotspot-iptables
# echo 1 > /proc/sys/net/ipv4/ip_forward
#dnsmasq -C /dev/null >/dev/null 2>&1 --bind-interfaces --listen-address=10.2.0.1 --dhcp-range=10.2.0.10,10.2.0.200,12h

#networkmanager
cat > /etc/sysconfig/network-scripts/ifcfg-Hotspot << FOE
#HWADDR=E8:B1:FC:7F:43:7E
ESSID="VIDEOS"
MODE=Ap
CHANNEL=11
KEY_MGMT=WPA-PSK
SECURITYMODE=open
WPA_ALLOW_WPA2=yes
CIPHER_PAIRWISE=CCMP
CIPHER_GROUP=CCMP
MAC_ADDRESS_RANDOMIZATION=default
TYPE=Wireless
BOOTPROTO=shared
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=no
NAME=Hotspot
UUID=1fc031d2-05e3-4b2b-a672-8a7df698fdc1
ONBOOT=yes
FOE

cat > /etc/sysconfig/network-scripts/Keys-Hotspot << FOE
WPA_PSK='videow123'
FOE

nmcli c up Hotspot
#nmcli d wifi hotspot ifname wlp1s0 con-name Hotspot ssid VIDEOS band bg channel 11 password videos123 

#firewall config                                                  
firewall-cmd --add-service=ssh --permanent                        
firewall-cmd --add-service=http --permanent                       
firewall-cmd --add-service=https --permanent                      
firewall-cmd --add-service=samba-client --permanent 



%end
