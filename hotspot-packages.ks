%packages

# Exclude unwanted groups that fedora-live-base.ks pulls in
-@dial-up
-@input-methods
-@standard
-@fonts
-@firefox
-@guest-desktop-agents
#-@networkmanager-submodules
-abrt-*
-autofs
-reiserfs-utils
-gfs2-utils


#Fonts
abattis-cantarell-fonts
liberation-mono-fonts                                            
liberation-sans-fonts                                            
liberation-serif-fonts                                           
terminus-fonts-console                                           

#stuff from workstation ks
#for production image, enable chromium,printing,hardware support,multimedia,networkmanager-submodules ---------------  disable desktop guests

@gnome-desktop
@hardware-support
#@multimedia
@printing

#for some reason the -submodule group above is no longer working.
NetworkManager-wifi
-NetworkManager-openvpn*
-NetworkManager-vpnc*
-NetworkManager-pptp*
-NetworkManager-ssh*
-NetworkManager-wwan*
-NetworkManager-adsl*
-NetworkManager-openconnect-gnome
-openconnect

#troubleshooting (comment out for prod)
#dconf-editor
#gnome-tweaks

#extra gnome custom stuff
xdg-user-dirs   #needed to create folders in home directory
evopop-gtk-theme
paper-icon-theme
f28-backgrounds-base
#f28-backgrounds-extras-base
#f28-backgrounds-gnome
#f28-backgrounds-extras-gnome
gnome-shell-extension-dash-to-dock
gnome-shell-extension-user-theme
gnome-shell-extension-apps-menu
gnome-shell-extension-places-menu

-glibc-all-langpacks
glibc-minimal-langpack

#Add a browser
#firefox
#google-chrome-ustable
#chromium
#midori
epiphany

#add suuport for efi boot
shim
grub2-efi

#fix liveuser acccount creation error
authselect-compat

nfs-utils
net-tools
hostapd
dnsmasq
#dbus-x11
wireless-tools
samba
samba-common-tools
nginx

#Remove even more stuff
-aajohan-comfortaa-fonts
-gnome-boxes
-rhythmbox
-libreoffice-*
-gnome-music
-httpd
-baobab
-gnome-calendar
#-sane-*
-evince
-flatpak
-gnome-photos
-gnome-maps
-simple-scan
-gnome-calculator
-gnome-clocks
-gnome-weather
-gnome-fonts
-gnome-documents
-gnome-contacts
-gnome-color-manager
-gnome-disk-utility
-gnome-screenshot
-gnome-logs
-gnome-font-viewer
-gnome-characters
-gnome-system-monitor
-file-roller-nautilus
-file-roller
-cheese
-totem
-anaconda #we don't want any installation for now
-@anaconda-tools
-lohit-*  #some kinda chinese fonts???
#-fcoe-utils
-man-pages-*
-aspell-*
-PackageKit*
-policycoreutils-gui
-man-db
-hplip-*
-flatpak
-gnome-software
-evince-nautilus
-gnome-initial-setup
-totem-nautilus
-libsane-*
-sane-backends-drivers-*
#need this
sane-backends-libs
-xdg-desktop-portal
-xdg-desktop-portal-gtk
-gnome-user-share
-orca
-vpnc*
-openvpn

%end

