# Fedora hotspot for sharing vids
# http:/www.coastsystems.net
#
# Maintainer(s):
# - Boyd Kelly       <bkelly AT coastsystems  DOT .net>


%include fedora-live-base.ks
%include fedora-live-minimization.ks

# Custom
%include extrarepos.ks
%include lang.ks

# packages
%include hotspot-packages.ks


# machine specific
%include y2.ks
%include kvm-guest.ks

# settings
%include winmount.ks
%include hotspot-config.ks

%include post-nochroot.ks


services --enabled=NetworkManager,ModemManager,sshd,nginx,smb,nmb,dnsmasq --disabled=network

#
# Disable this for now as packagekit is causing compose failures
# by leaving a gpg-agent around holding /dev/null open.
#
#include snippets/packagekit-cached-metadata.ks

part / --size 5120 --fstype ext4
timezone --utc UTC
selinux --disabled

%post

cat >> /etc/rc.d/init.d/livesys << EOF

# disable gnome-software automatically downloading updates
cat >> /usr/share/glib-2.0/schemas/org.gnome.software.gschema.override << FOE
[org.gnome.software]
download-updates=false
FOE

# don't autostart gnome-software session service
rm -f /etc/xdg/autostart/gnome-software-service.desktop

# disable the gnome-software shell search provider
cat >> /usr/share/gnome-shell/search-providers/org.gnome.Software-search-provider.ini << FOE
DefaultDisabled=true
FOE

# don't run gnome-initial-setup
mkdir ~liveuser/.config
touch ~liveuser/.config/gnome-initial-setup-done

# suppress anaconda spokes redundant with gnome-initial-setup
cat >> /etc/sysconfig/anaconda << FOE
[NetworkSpoke]
visited=1

[PasswordSpoke]
visited=1

[UserSpoke]
visited=1
FOE

# make the installer show up
if [ -f /usr/share/applications/liveinst.desktop ]; then
  # Show harddisk install in shell dash
  sed -i -e 's/NoDisplay=true/NoDisplay=false/' /usr/share/applications/liveinst.desktop ""
  # need to move it to anaconda.desktop to make shell happy
  mv /usr/share/applications/liveinst.desktop /usr/share/applications/anaconda.desktop

  #this will be customized in the included desktop file anyway
#cat >> /usr/share/glib-2.0/schemas/org.gnome.shell.gschema.override << FOE
#[org.gnome.shell]
#favorite-apps=['org.gnome.Terminal.desktop', 'wine.desktop', 'wine-winecfg.desktop', 'wine-uninstaller.desktop', 'org.gnome.Nautilus.desktop', 'anaconda.desktop']
#FOE


  # Make the welcome screen show up
  #if [ -f /usr/share/anaconda/gnome/fedora-welcome.desktop ]; then
  #  mkdir -p ~liveuser/.config/autostart
  #  cp /usr/share/anaconda/gnome/fedora-welcome.desktop /usr/share/applications/
  #  #don't have this show up on login, just available in the menus/dock/dash whatever
  #  cp /usr/share/anaconda/gnome/fedora-welcome.desktop ~liveuser/.config/autostart/
  #fi

  # Copy Anaconda branding in place
  if [ -d /usr/share/lorax/product/usr/share/anaconda ]; then
    cp -a /usr/share/lorax/product/* /
  fi
fi

# rebuild schema cache with any overrides we installed
glib-compile-schemas /usr/share/glib-2.0/schemas

# set up auto-login
cat > /etc/gdm/custom.conf << FOE
[daemon]
AutomaticLoginEnable=True
AutomaticLogin=liveuser

FOE

# Turn off PackageKit-command-not-found while uninstalled
if [ -f /etc/PackageKit/CommandNotFound.conf ]; then
  sed -i -e 's/^SoftwareSourceSearch=true/SoftwareSourceSearch=false/' /etc/PackageKit/CommandNotFound.conf
fi


# make sure to set the right permissions and selinux contexts
chown -R liveuser:liveuser /home/liveuser/
restorecon -R /home/liveuser/

EOF

mount -t nfs4 localhost:/bkelly/ /mnt/tmp
echo $(date -I)-wtl-remix  > /mnt/tmp/livecd/wtl-remix/install.log

%end
