iwconfig wlp1s0 essid videos mode master
ip address dev wlp1s0 10.2.0.1 netmask 255.255.255.0
ifconfig wlp1s0 10.2.0.1 netmask 255.555.255.0 broadcast 10.2.0.255 up
# iptables-restore < saved-hotspot-iptables
# echo 1 > /proc/sys/net/ipv4/ip_forward
dnsmasq -C /dev/null >/dev/null 2>&1 --bind-interfaces --listen-address=10.2.0.1 --dhcp-range=10.2.0.10,10.2.0.200,12h

#
