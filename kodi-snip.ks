useradd -G wheel,audio,video kodi
cat  >/etc/systemd/system/kodi.service <<FOE
[Unit]
Description = kodi-standalone using xinit
After = remote-fs.target systemd-user-sessions.service
 
[Service]
User = kodi
Group = kodi
PAMName = login
Type = simple
ExecStart = /usr/bin/xinit /usr/bin/dbus-launch /usr/bin/kodi-standalone -- :0 -nolisten tcp
Restart = on-abort
 
[Install]
WantedBy = multi-user.target
FOE

cat > /usr/share/xsessions/kodi.desktop << FOE
[Desktop Entry]
Name=Kodi
Comment=This session will start Kodi media center
Exec=kodi-standalone
TryExec=kodi-standalone
Type=Application
FOE

mkdir -p /home/liveuser/.config/autostart
cp /usr/share/xsessions/kodi.desktop /home/liveuser/.config/autostart/
#cp /usr/share/applications/kodi.desktop /home/liveuser/.config/autostart/
echo "X-GNOME-Autostart-enabled=true" >> /home/liveuser.config/autostart/kodi.desktop
cat > /home/liveuser/pkvids << FOE
<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<smartplaylist type="movies">
    <name>Sophie et Caleb</name>
    <match>all</match>
    <rule field="file" operator="contains">
        <value>pk</value>
    </rule>
    <limit>1</limit>
    <order direction="ascending">random</order>
</smartplaylist>
FOE

cat > /home/liveuser/guere << FOE
<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<smartplaylist type="movies">
    <name>Sophie et Caleb</name>
    <match>all</match>
    <rule field="file" operator="contains">
        <value>GUR</value>
    </rule>
    <limit>1</limit>
    <order direction="ascending">random</order>
</smartplaylist>
FOE
