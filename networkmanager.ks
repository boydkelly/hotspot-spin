cat > /etc/sysconfig/network-scripts/ifcfg-Hotspot << FOE
#HWADDR=E8:B1:FC:7F:43:7E
ESSID="VIDEOS"
MODE=Ap
CHANNEL=11
KEY_MGMT=WPA-PSK
SECURITYMODE=open
WPA_ALLOW_WPA2=yes
CIPHER_PAIRWISE=CCMP
CIPHER_GROUP=CCMP
MAC_ADDRESS_RANDOMIZATION=default
TYPE=Wireless
BOOTPROTO=shared
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=no
NAME=Hotspot
UUID=1fc031d2-05e3-4b2b-a672-8a7df698fdc1
ONBOOT=yes
FOE

cat > /etc/sysconfig/network-scripts/Keys-Hotspot << FOE
WPA_PSK='videos123'
FOE

nmcli c up Hotspot
#nmcli d wifi hotspot ifname wlp1s0 con-name Hotspot ssid VIDEOS band bg channel 11 password videos123 
