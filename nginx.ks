cat > /etc/nginx/default.d/videos.conf << FOE
server {
	listen 80 default_server;
	listen [::]:80 default_server ipv6only=on;
	server_name videos.localhost www.videos.localhost;
	location / {
	  root /home/liveuser/Videos;
	  index index.html index.htm;
	}
}
FOE

sed -i 's/^i[[:space:]]root/\t\troot\t\/home\/liveuser\/Videos/' /etc/nginx/default.conf
