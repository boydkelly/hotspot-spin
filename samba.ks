cat > /etc/samba/smb.conf << FOE
[global]
 workgroup = bouake
 security = user
 share modes = yes
 null passwords = yes

 [videos]
 path = /home/liveuser/Videos/ 
 public = yes
 writable = yes
 comment = smb share
 printable = no
 guest ok = yes
FOE

cat > /etc/samba/smbusers << FOE
liveuser
videos
FOE
